//
//  ContentView.swift
//  CIExample
//
//  Created by Guillermo Ignacio Enriquez Gutierrez on 2020/05/21.
//  Copyright © 2020 Guillermo Ignacio Enriquez Gutierrez. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World from SwiftUI!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
